<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserAuthController extends Controller
{
    public function register(Request $request)
    {
        $login = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed'
        ]);

        $login['password'] = bcrypt($request->password);

        $user = User::create($login);

        $token = $user->createToken('API Token')->accessToken;

        return response([ 'user' => $user, 'token' => $token]);
    }

    public function login(Request $request)
    {
        $login = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);

        if (!auth()->attempt($login)) {
            $error = 'Credenciais inválidas';
            $response = [ 'error' => $error];
            return response()->json($response, 401);
        }

        $token = auth()->user()->createToken('API Token')->accessToken;
        return response()->json(200);
}
}
