<?php

namespace App\Imports;

use App\Models\Employee;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpsert;



class import implements ToModel, WithValidation, WithHeadingRow, WithUpserts
{
    use Importable;

    public function model(array $row)
    {
        return new Employee([
            'name' => $row['name'],
            'email' => $row['email'],
            'document' => $row['document'],
            'city' => $row['city'],
            'state' => $row['state'],
            'startDate' => $row['start_date'],
        ]);
    }
    public function uniqueBy()
    {
        return 'document';
    }


}
